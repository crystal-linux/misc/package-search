# package-search
Web UI for viewing info about Crystal Linux packages

## Depends:
* `python-pip`
* `rsync`
* `wget`
* `pip3 install -r requirements.txt` (or venv lol)

## Regenerate data:
* Configure `settings.toml`
* Have an arch mirror either local, or remote ssh-able from host server with key
* `python3 data_setup.py`

## Credits:
We've modified templates, CSS, and JS from https://github.com/flipflop97/WebMan