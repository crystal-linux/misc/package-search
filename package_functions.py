# Stdlib
import os, sys

# Pip
import toml

# Custom
# n/a

if not os.path.exists("settings.toml"):
    print("No such file: settings.toml")
    sys.exit(1)

if not os.path.exists("data.toml"):
    print("You haven't run 'data_setup.py'")
    sys.exit(1)

settings = toml.loads(open("settings.toml").read())
data = toml.loads(open("data.toml").read())


def list_repos():
    return data.keys()


def list_packages_in(repo):
    if repo in data.keys():
        stuff = []
        for element in data[repo]:
            stuff.append(data[repo][element])
        return stuff
    else:
        return []


def search_for(package):
    # print("LIB SEARCH FOR " + package)
    found = []
    names_simple = []
    for repo in data.keys():
        for pkg in data[repo]:
            if package in pkg or pkg in package:
                if pkg not in names_simple:
                    pdat = data[repo][pkg]
                    found.append(pdat)
                    names_simple.append(pkg)
    return found

def get_by_name(package):
    for repo in data.keys():
        for pkg in data[repo]:
            if pkg == package:
                return data[repo][pkg]
    return None


# print(str(search_for('crystal')))
list_packages_in("any")