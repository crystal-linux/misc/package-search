# PyPi
from flask import Flask, render_template, redirect

# Custom
from package_functions import settings, list_repos, list_packages_in, search_for
from util import getShortcutIcon, parsePackage, searchPackages

app = Flask(__name__)

base = settings["data_dir"]
url = settings["data_url"]

PORT = 8989

if settings["use_proxy"]:
    base_url = settings["proxy_url"]
else:
    base_url = f"http://127.0.0.1:{str(PORT)}"

@app.route("/")
def index():
    return render_template(
        "message.html",
        base=base_url,
        message="Welcome to " + settings["name"] + "<br/><p>Perhaps you'd like to see our list of <a href='/repos'>repos</a>?</p>",
        title=settings["name"],
    )


@app.route("/repos")
def show_repos():
    msg = "<p>We have the following repos:</p>"
    for repo in list_repos():
        msg += f"<p><a href='{base_url}/packages/{repo}'>{repo}</a></p>"
    return render_template('message.html', base=base_url, message=msg, title=settings['name'])


@app.route("/packages/<repo>")
def show_pkg_of(repo):
    results = list_packages_in(repo)
    if not results:
        return render_template(
            "message.html",
            base=base_url,
            message=f"Couldn't find anything for: {repo}",
            title=settings["name"],
        )
    else:
        fpackages = []
        ids = []
        for item in results:
            if item["pkgname"] not in ids:
                fpackages.append(parsePackage(item))
                ids.append(item["pkgname"])
        return render_template(
            "packages.html",
            base=base_url,
            searchterm=f"packages in: {repo}",
            packages=fpackages,
            title=settings['name'] + ": Packages in" + repo 
        )


@app.route("/search/<name>")
def dsearch_for(name):
    # print("Searching for " + name)
    results = search_for(name)
    fpackages = []
    ids = []
    for item in results:
        if item["pkgname"] not in ids:
            fpackages.append(parsePackage(item))
            ids.append(item["pkgname"])
    fpackages += searchPackages(name)
    st = name
    if len(results) == 0:
        st += "<br/>FYI, none of the below results were found in the Crystal repo"
    return render_template(
        "packages.html",
        base=base_url,
        searchterm=st,
        packages=fpackages,
        title=settings['name'] + ": " + name
    )

@app.route('/icon/<pkgname>')
def icon(pkgname):
    pkgname = pkgname.lower()
    icon = getShortcutIcon(pkgname)
    if icon is None:
        return ''
    return redirect(icon)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=PORT, debug=True)
