from urllib3 import PoolManager
from urllib.parse import urljoin
from certifi import where
from bs4 import BeautifulSoup
from editdistance import eval as levdist
from json import loads

from package_functions import get_by_name

def loadJson(url):
    pool = PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=where())
    request = pool.request('GET', url, timeout=10)
    return loads(request.data)


def parsePackage(package, search=''):
    if 'pkgname' in package:
        aur = False
        name = package['pkgname']
        if 'pkgdesc' in package.keys():
            desc = package['pkgdesc']
        else:
            desc = None
        ver = package['pkgver']
        url = package['url']

    else:
        aur = True
        name = package['Name']
        desc = package['Description']
        ver = package['Version']
        url = package['URL']

    expand = search == name

    return name, desc, ver, url, expand, aur

def getShortcutIcon(pkgname):
    try:
        return getShortcutIcon.cache[pkgname]
    except AttributeError:
        getShortcutIcon.cache = {}
    except KeyError:
        try:
            packageUrl = parsePackage(get_by_name(pkgname))[3]
            pool = PoolManager(cert_reqs='CERT_REQUIRED', ca_certs=where())
            pageRequest = pool.request('GET', packageUrl, timeout=3.0)
            pageContent = pageRequest.data
            pageSoup = BeautifulSoup(pageContent, 'html.parser')
            iconLink = pageSoup.find('link', rel='icon')['href']
            getShortcutIcon.cache[pkgname] = urljoin(packageUrl, iconLink)
        except:
            try:
                getShortcutIcon.cache[pkgname] = urljoin(packageUrl, '/favicon.ico')
            except:
                getShortcutIcon.cache[pkgname] = None

    return getShortcutIcon(pkgname)

def searchPackages(name):
    results = loadJson('https://www.archlinux.org/packages/search/json/?q=%s' % name)['results']
    results = sorted(results, key=lambda x: levdist(name, x['pkgname']))[:100]
    packages = [parsePackage(package, name) for package in results if package['arch'] in ('x86_64', 'any')]

    results = loadJson('https://aur.archlinux.org/rpc/?v=5&type=search&arg=%s' % name)['results']
    results = sorted(results, key=lambda x: levdist(name, x['Name']))[:100]
    packages += [parsePackage(package, name) for package in results]

    packages = sorted(packages, key=lambda x: levdist(name, x[0]))[:100]
    return packages