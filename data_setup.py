# system
import os, subprocess, sys, shutil

# pip
import toml

# custom
# n/a?

def parse_package_info(file_content):
    # Split the content by lines
    lines = file_content.strip().splitlines()

    package_info = {}
    key = None

    for line in lines:
        # Check if the line starts and ends with '%' which indicates a key
        if line.startswith('%') and line.endswith('%'):
            # Extract the key, remove '%' and convert to lowercase
            key = line.strip('%').lower()
        else:
            # Assign the value to the corresponding key in the dictionary
            if key:
                package_info[key] = line
                key = None  # Reset key for the next pair

    return package_info

if os.path.exists("settings.toml"):
    settings = toml.loads(open("settings.toml").read())
else:
    print("No such file: settings.toml")
    sys.exit(1)

url = settings["data_url"]
base = settings["data_dir"]

if not os.path.exists(base):
    os.makedirs(base, exist_ok=True)

repo_names = ['crystal-x86_64', 'crystal-any']

data = {}

"""
data = {
    "any": {
        "crystal-core" : {},
    },
    "x86_64": {
        "amethyst": {},
    },
}
"""

for repo in repo_names:
    print(f"Working on {repo}")

    if os.path.exists(".tempdir"):
        shutil.rmtree(".tempdir", ignore_errors=True)
    os.system("mkdir .tempdir")
    os.chdir(".tempdir")

    os.system(f"wget {url}/{repo}/{repo}.db")
    os.system(f"tar -xf {repo}.db")
    os.system(f"rm {repo}.db") # so it doesn't trip up the below loop

    repo_base_path = f"{base}/{repo}"

    data[repo] = {}

    for thing in os.listdir():
        print("Found " + thing)
        info_file = open(thing+"/desc").read()
        pkg_data = parse_package_info(info_file)
        data[repo][thing] = pkg_data

        with open("../data.toml", "w") as f:
            f.write(toml.dumps(data))

    os.chdir("..")
