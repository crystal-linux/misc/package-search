function search() {
    var pkgname = document.getElementById('searchbar').value;
    if (pkgname != '')
        window.location = '/search/' + pkgname;
}

function expand(id) {
    var package = document.getElementById('package-' + id);
    package.classList.toggle('expanded');
}

function showIcon(id) {
    document.getElementById('icon-' + id).style.opacity = 1.0;
    document.getElementById('iconph-' + id).style.opacity = 0.0;
}